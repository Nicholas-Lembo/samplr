#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <sndfile.h>
#include <jack/jack.h>
#include <jack/midiport.h>
#include <jack/ringbuffer.h>
#include "samplr.h"
#define DEFAULT_RB_SIZE 16384
//jack
jack_port_t *audio_in_l;
jack_port_t *audio_in_r;
jack_port_t *midi_out;
jack_default_audio_sample_t *left_sample = NULL;
jack_default_audio_sample_t *right_sample = NULL;
jack_nframes_t nframes;
const size_t sample_size = sizeof(jack_default_audio_sample_t);

//SNDFILE
SF_INFO *sf_info;

//args
int start_octave, end_octave, vel_inc, key_off, tail_end;
char path[1024];
char name[1024];



//sync
jack_ringbuffer_t *rb;
pthread_mutex_t disk_thread_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  data_ready = PTHREAD_COND_INITIALIZER;


static int process(jack_nframes_t nframes, void *arg){
  thread_info_t *info = (thread_info_t *) arg;
  if((!info->can_process) || (!info->can_capture)) return 0;
  void *midi_out_buff = jack_port_get_buffer(midi_out, nframes);
  unsigned char *midi_event_buffer; 
  return 0;
}

void *disk_thread(void *arg){
  thread_info_t *info = (thread_info_t *)arg;
  jack_nframes_t total = 0;
  void *framebuffer = malloc(2 * sample_size);
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  pthread_mutex_lock(&disk_thread_lock);
  strcpy(output_filename, path);
  strcat(output_filename, "_C-");
  itoa(start_octave, octave_number, 10)
  strcat(output_filename, octave_number);
  info->sf = sf_open(path, SFM_WRITE, sf_info);
  for(;;){
    while(info->can_capture && (jack_ringbuffer_read_space(rb) >= bytes_per_frame)){
      jack_ringbuffer_read(rb, framebuffer, bytes_per_frame);
      if ()
      
    }
    pthread_cond_wait(&data_ready, &disk_thread_lock); 
  } 
}




void setup_ports(thread_info_t *info){
  rb = jack_ringbuffer_create(2 * sample_size * info->rb_size);
  audio_in_r = jack_port_register(info->client, "samplr_right", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  audio_in_l = jack_port_register(info->client, "samplr_left", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  info->can_process = 1;
}


void setup_file_writer(thread_info_t *info){
  info->sf_info = (SF_INFO*) malloc(sizeof(SF_INFO));
  info->sf_info->samplerate = jack_get_samplerate(info->client);
  info->sf_info->channels = 2;
  info->sf_info->format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
  info->duration = (key_off + tail_end) * info->sf_info->samplerate;
  pthread_create(&info->disk_thread_id, NULL, disk_thread, info);
}

int main(int argc, char **argv){
  thread_info_t info;
  memset(&info, 0, sizeof(info));
  info.rb_size = DEFAULT_RB_SIZE;
  info.client = jack_client_open("samplr", JackNullOption, NULL);
  jack_set_process_callback(info.client, process, (void*)(&info)); 
  setup_ports(&info);
  jack_activate(info.client);
  fprintf(stdout, "Press a key to start");
  getchar();
  setup_file_writer(&info); 
  info.can_capture = 1;
  pthread_join(info->thread_info, NULL); //wait until the disk thread is complete
  jack_ringbuffer_free(rb);
  jack_client_close(info.client);
  return 0;
}
