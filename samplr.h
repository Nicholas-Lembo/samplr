//thread_info_t
typedef struct {
  pthread_t disk_thread_id;
  pthread_t screen_thread_id;
  jack_nframes_t rb_size;
  jack_client_t *client;
  SNDFILE *sf;
  int duration;
  volatile int can_capture;
  volatile int can_process;
  volatile int status;
}
thread_info_t;

static int process(jack_nframes_t nframes, void *arg);
void *disk_thread(void *arg);
void setup_ports(thread_info_t *info);
void setup_file_writer(thread_info_t *info);
int screen_init();
void *screen_thread(void *arg);

//global variables
int current_note;
char output_filename[1024];
char octave_number[64];
const char* notes = {"C", "C#", "D#", "E", "F", "G", "G#", "A", "A#", "B"};
